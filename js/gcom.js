function openAdult()
{
    $('#myModal').modal('show');
}
function closeAdult(){
    $('#myModal').modal('hide');
}
function reserve_1()
{
    var p = {
        "agency":((typeof flight_to_reserve.id_agency !== 'undefined')?flight_to_reserve.id_agency:flight_to_reserve.id_gohar),
        "flight" :((typeof flight_to_reserve.id_flight !== 'undefined')?flight_to_reserve.id_flight:flight_to_reserve.tbl_flight_id),
        "ncap":flight_to_reserve.nCap,
        "adult":$("#adult").val(),
        "child":$("#child").val(),
        "inf":$("#inf").val(),
        "export":"json",
        "submit":"submit"

    };
    console.log(p);
    $("#khoon").html('<img src="img/status_fb.gif" />');
    $("#sabt_reserve").prop("disabled",true);
    $.post('reserve_level1.php',p,function(result){
        console.log(result);
        var res_arr = JSON.parse(result);
        var res = res_arr.ticket[0];
        console.log(res_arr);
        if(parseInt(res.status,10)===1)
        {
            //window.location = "info.php?voucher_id="+res.voucher_id+"&total="+res.total+"&aim="+res.aim+"&adult="+$("#adult").val()+"&child="+$("#child").val()+"&inf="+$("#inf").val()+"&";
            var reserve_data_1 = {
                "flight" : flight_to_reserve,
                "adult" : $("#adult").val(),
                "child": $("#child").val(),
                "inf": $("#inf").val(),
                "reserve_1" : res_arr
            };
            $("#reserve_data").val(JSON.stringify(reserve_data_1));
            $("#frm1").submit();
        }
        else
        {
            $("#khoon").html('');
            $("#sabt_reserve").prop("disabled",false);
            alert('در رزرو خطایی رخ داد لطفا مجددا سعی نمایید');
            closeAdult();
        }
    }).fail(function(){
        $("#khoon").html('');
        $("#sabt_reserve").prop("disabled",false);
        alert('در ارتباط با سرور مشکلی پیش آمد ، لطفت مجددا سعی نمایید');
    });
}
function print_ticket(voucher_id)
{
    obj={"voucher_id":voucher_id};
    $.get("ticket_curl.php",obj,function(result){
        $("#bank_back_body").html(result);
    });
}
