<?php
    include_once('page/header.php');  
?>
        <div class="container">
            <div class="row clearfix">
                <div class="col-md-12 col-lg-12 column">
                    <div class="row clearfix">
                        <!-- شروع باکس سمت راست-->
                        <div class="col-md-4 col-lg-4 col-xs-12 column pull-right">
                            <div class="row clearfix" style="padding-left:7px;margin-top: 12px;">
                                <div class="tabbable" id="tabs-207930">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">اطلاعات پرواز</h3>
                                        </div>
                                        <div class="panel-body" style="background-color: white;border: 1px solid #000;">
                                            <section class="s-form">
                                                <table class="table table-bordered table-responsive">
                                                <tbody>
                                                    <tr>
                                                        <td>مبدا</td>
                                                        <td>مشهد</td>
                                                    </tr>
                                                    <tr>
                                                        <td>مقصد</td>
                                                        <td>تهران</td>
                                                    </tr>
                                                    <tr>
                                                        <td>نام ایرلاین</td>
                                                        <td>گوهر</td>
                                                    </tr>
                                                    <tr>
                                                        <td>تاریخ پرواز</td>
                                                        <td>1/1/1394</td>
                                                    </tr>
                                                    <tr>
                                                        <td>روز پرواز</td>
                                                        <td>شنبه</td>
                                                    </tr>
                                                    <tr>
                                                        <td>ساعت پرواز</td>
                                                        <td>11:33</td>
                                                    </tr>
                                                    <tr>
                                                        <td>شماره پرواز</td>
                                                        <td>666</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
<div class="col-md-8 col-lg-8 col-xs-12 column" style="margin-top: 12px;">
    <div class="row clearfix" style="padding-left:20px;padding-right:7px;min-height:763px;">
        <div class="panel panel-default">
            <div class="panel-heading ph_bgcolor">
                <table class="table table-bordered table-responsive" style="margin:0px;padding:0px;border: none;background-color: none !important;">
                    <thead style="border: none;background-color: none !important;">
                        <tr style="border: none;background-color: none !important;">
                            <td style="border: none;background-color: none !important;">صدور بلیط</td>
                            <td style="border: none;background-color: none !important;"></td>
                            <td class="red-box">زمان باقیمانده رزرو 05:00</td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="panel-body pd_bgcolor">
            <form method="" name="" id="" accept="" action="">
                <table class="table table-bordered table-responsive" style="margin:0px;padding:0px;margin-top: 3px;">
                    <thead>
                        <tr>
                            <!--th class="Theader">&nbsp;ایرلاین&nbsp;/&nbsp;شماره&nbsp;پرواز&nbsp;/&nbsp;ساعت&nbsp;پرواز</t-->
                            <th>ردیف</th>
                            <th>نام</th>
                            <th>نام خانوادگی</th>
                            <th colspan="6"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>جلال</td>
                            <td>محمدزاده</td>
                            <th colspan="6">
                                <span><button class="btn btn-success">چاپ بلیط</button></span>
                                <span><button class="btn btn-success">دریافت PDF</button></span>
                                <span><button class="btn btn-success">ارسال ایمیل</button></span>
                            </th>
                        </tr>
                    </tbody>
                </table>
                
            </form>
            </div>
        </div>
    </div>
</div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <!-- اتمام باکس سمت چپ-->  
                    </div>
                </div>
            </div>
        </div>  
                        
<?php
    include_once('page/footer.php');
?>