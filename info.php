<?php
    include_once('page/header.php');
    $GLOBALS['cont']=0;
    function generateOption($inp,$start,$selected=-1)
    {
        $ou='';
        for($i=$start;$i<=$inp;$i++)
        {
            $ou.='<option '.($i==$selected?'selected="selected"':'').' value="'.$i.'">'.$i.'</option>';
        }
        return($ou);
    }
    function generateField($inp,$tarikh,$age_select,$age)
    {
        $ou='';
        for($i=0;$i<$inp;$i++)
        {
            $ou.='<tr>';
            $ou.='<td>'.($GLOBALS['cont']+1).'</td>';
            $ou.='<td>'.$age.'<input type="hidden" name=\'mosafer[age][]\' value="'.$age.'" ></td>';
            $ou.='<td><select id="'.$age.'_age_'.$i.'" name=\'mosafer[sex][]\' >'.$age_select.'</select></td>';
            $ou.='<td><input type="text" id="'.$age.'_fname_'.$i.'" name=\'mosafer[name][]\' ></td>';
            $ou.='<td><input type="text" id="'.$age.'_lname_'.$i.'" name=\'mosafer[family][]\' ></td>';
            $ou.='<td><input type="text" id="'.$age.'_melli_'.$i.'" name=\'mosafer[code_melli][]\' ></td>';
            $ou.='<td>'.$tarikh.'</td>';
            $ou.='</tr>';
            $GLOBALS['cont']++;
        }
        return($ou);
    }
    //var_dump($_REQUEST['reserve_data']);
    $all = json_decode(str_replace('\\"','"',$_REQUEST['reserve_data']));
    $flight = $all->flight;
    //var_dump($all->reserve_1->ticket[0]);
    $age_select ='<option value="1">
                    مرد
                </option>
                <option value="0">
                    زن
                </option>';
    $tarikh='
        <select name=\'mosafer[birthday_day][]\' >
            <option value="-1" >روز</option>
            '.generateOption(31,1).'
        </select>
        <select name=\'mosafer[birthday_month][]\' >
            <option value="-1" >ماه</option>
            '.generateOption(12,1).'
        </select>
        <select name=\'mosafer[birthday_year][]\' >
            <option value="-1" >سال</option>
            <'.generateOption(1395,1300).'
        </select> 
            ';
    $ou = generateField($all->adult,$tarikh,$age_select,'Adult');
    $ou .= generateField($all->child,$tarikh,$age_select,'Child');
    $ou .= generateField($all->inf,$tarikh,$age_select,'Inf');
    //$adl = generateField($all->child,$tarikh,$age_select,'Child');
?>
<div class="container">
    <div class="col-lg-2 gc-border gc-gray gc-margin gc-padding" >
        اطلاعات پرواز
    </div>
    <div class="col-lg-9 gc-border gc-gray gc-margin gc-padding" >
        اطلاعات مسافران
    </div>
    <div class="col-lg-2 gc-border gc-margin gc-padding" >
        <div class="gc-padding" >
            مبدا:
            <?php echo $flight->source_city; ?>
        </div>
        <div class="gc-padding" >
            مقصد:
            <?php echo $flight->des_city; ?>
        </div>
        <div class="gc-padding" >
            نام ایرلاین:
            <?php echo $flight->airline; ?>
        </div>
        <div class="gc-padding" >
            تاریخ پرواز:
            <?php echo $flight->date; ?>
        </div>
        <div class="gc-padding" >
            ساعت پرواز:
            <?php echo $flight->time; ?>
        </div>
        <div class="gc-padding" >
            شماره پرواز:
            <?php echo $flight->tbl_flight_id; ?>
        </div>
    </div>
    <div class="col-lg-9 gc-border gc-margin gc-padding" >
        <form method="post" action="reserve_level15.php">
        <table class="table table-bordered gc-table">
            <tr>
                <th>
                    ردیف
                </th>
                <th>
                    سن
                </th>
                <th>
                    جنسیت
                </th>
                <th>
                    نام
                </th>
                <th>
                    نام خانوادگی
                </th>
                <th>
                    کدملی
                </th>
                <th>
                    تاریخ تولد
                </th>
            </tr>
            <?php
                echo $ou;
            ?>
        </table>
            <input type="hidden" name="submit" value="submit" >
            <input type="hidden" name="voucher" value="<?php echo $all->reserve_1->ticket[0]->voucher_id; ?>" >
            <input type="hidden" name="aim" value="<?php echo $all->reserve_1->ticket[0]->aim; ?>" >
            <input type="hidden" name="agency_site" value="<?php echo $flight->site; ?>" >
            <input type="hidden" name="ticket_id" value="<?php echo implode(',',$all->reserve_1->ticket_id); ?>" >
            <input type="hidden" name="reserve_data" value='<?php echo $_REQUEST['reserve_data']; ?>' >
            <div class="row gc-margin">
                <div class="col-lg-6" >
                تلفن همراه: 
                <input name="mobile" type="text"  >
                </div>
                <div class="col-lg-6" >
                ایمیل: 
                <input name="email" type="text"  >
                </div>
            </div>
            <div class="gc-margin">
                <div class="row gc-margin gc-padding">
                    <div class="col-lg-4">
                        مبلغ کل:
                        <?php echo ($all->reserve_1->ticket[0]->total+$all->reserve_1->ticket[0]->offer); ?>
                         ریال
                    </div>
                    <div class="col-lg-4">
                        تخفیف :
                        <?php echo $all->reserve_1->ticket[0]->offer; ?>
                         ریال
                    </div>
                    <div class="col-lg-4">
                        مبلغ قابل پرداخت:
                        <?php echo $all->reserve_1->ticket[0]->total; ?>
                         ریال
                    </div>
                </div>    
                <button class="btn btn-default">
                    ثبت پرواز
                </button>
                <a class="btn btn-default" href="index.php"  >لغو پرواز</a>
            </div>
        </form>
    </div>
</div>                
<?php
    include_once('page/footer.php');
?>